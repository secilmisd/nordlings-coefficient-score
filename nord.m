data = datastruct.Dataset.fetch('Tjarnberg-ID1014949-D20151111-N10-E30-SNR10-IDY1014949.json');
alpha = 0.01;

%%%% WORKS PROPERLY %%%%%

Pt = data.P;
LamF = 0.005*ones(size(Pt));
F = sqrt(LamF).*randn(size(Pt));
LamE = LamF;
Y = data.Y-data.sdY;
E = sqrt(LamE).*randn(size(Y));  
Y = Y +E;
P = Pt + F;
Gamma = RInorm(Y', -P', LamE', LamF', alpha)


%%%% DOES NOT WORK PROPERLY WITH THE DATA'S OWN NOISE %%%%%
%%%% ALL ELEMENTS OF THE RESULTING GAMMA MATRIX ARE ALMOST THE SAME AROUND 0.15 %%%%
%Pt = data.P;
%LamF = 0.005*ones(size(Pt));
%F = sqrt(LamF).*randn(size(Pt));
%LamE = data.sdY;
%Y = data.Y;
%P = Pt + F;
%Gamma = RInorm(Y', -P', LamE', LamF', alpha)
